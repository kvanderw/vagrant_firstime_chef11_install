#!/bin/bash

##########################
# Install Chef (11)
##########################
echo .
echo Installing Chef
echo .
dpkg -i /vagrant/chef-server_11.1.4-1_amd64.deb
chef-server-ctl reconfigure

##########################
# updated the OS
##########################
echo .
echo updating the OS
echo .
apt-get -y update

##########################
# add some of the basics
##########################
echo .
echo installing basic features of OS
echo .
apt-get -y install screen vim git tig

##########################
# build the disk index
##########################
echo .
echo building the disk index
echo .
updatedb

##########################
# get knife setup and ready to run
##########################
echo .
echo setting up knife for future use
echo .
cd ~vagrant
mkdir .chef
chown vagrant .chef
cd .chef
cat <<EOG > knife.rb
log_level                :info
log_location             STDOUT
node_name                'admin'
client_key               '/home/vagrant/.chef/admin.pem'
validation_client_name   'chef-validator'
validation_key           '/home/vagrant/chef/validation.pem'
chef_server_url          'https://chef-server2'
cache_type               'BasicFile'
cache_options( :path => '/home/vagrant/.chef/checksums' )
knife[:xenserver_username]  = "username"
knife[:xenserver_password]  = "password"
knife[:xenserver_host]      = "IP address"
cookbook_copyright          "Meridian Data Systems, Inc."
cookbook_email              "techsupport@meridian-ds.com"
cookbook_license            "none"
cookbook_path               ["/vagrant/cookbooks"]
current_dir                 = File.dirname(__FILE__)
EOG

cp /etc/chef-server/chef-validator.pem validation.pem
cp /etc/chef-server/admin.pem admin.pem
cp /etc/chef-server/chef-webui.pem webui.pem
chown vagrant *

##########################
# install all the basic cookbooks
##########################
echo .
echo installing bsaic cookbooks
echo .
exec sudo -i -u vagrant /bin/bash - <<EOF
cd /vagrant
git init
git add .
git commit -am "Initial Commit"
# echo .
# echo knfie cookbook site install apache
# echo .
# knife cookbook site install apache
# echo .
# echo knife cookbook site install mysql
# echo .
# knife cookbook site install mysql
# echo .
# echo knife cookbook site install vim
# echo .
# knife cookbook site install vim
echo .
echo knife cookbook site install screen
echo .
knife cookbook site install screen
# echo .
# echo knife cookbook site install nagios
# echo .
# knife cookbook site install nagios
# echo .
# echo knife cookbook site install apt
# echo .
# knife cookbook site install apt
# echo .
# echo knife cookbook site install motd-tail
# echo .
# knife cookbook site install motd-tail
# echo .
# echo knife cookbook site install sudo
# echo .
# knife cookbook site install sudo
# echo .
# echo knife cookbook site install users
# echo .
# knife cookbook site install users
# echo .
# echo knife cookbook site install ntp
# echo .
# knife cookbook site install ntp
# echo .
# echo knife cookbook site install git
# echo .
# knife cookbook site install git
# echo .
# echo knife cookbook site install tig
# echo .
# knife cookbook site install tig
# echo .
# echo knife cookbook site install rsyslog
# echo .
# knife cookbook site install rsyslog
# echo .
# echo knife cookbook site install screen
# echo .
# knife cookbook site install screen
# echo .
# echo knife cookbook site install varnish
# echo .
# knife cookbook site install varnish
# echo .
# echo knife cookbook site install memcache
# echo .
# knife cookbook site install memcache
# echo .
# echo knife cookbook site install memcached
# echo .
# knife cookbook site install memcached
# echo .
# echo # knife cookbook site install redis
# echo .
echo .
echo knife cookbook -a upload
echo .
knife cookbook -a upload
EOF

echo .
echo upgrading the OS
echo .
# apt-get -y upgrade
# reset
# clear
# reset
